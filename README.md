# azurearmtask

Create an Azure RM template which provisions the following services:



1) App Service Plan - hosting plan for application service

2) Application Insight

3) SQL server and a database - template for Sql server/database.

4) Service Bus - template for service bus, empty without queue and topics.

Above services should be added as a dependency for app service. Their parameters should be passed as app settings to Application Service (e.g: instrumentation key, connection strings).


5) ApplicationService - application service template (required input parameters should be hard coded; as an option you should be able to set own parameters, e.g: "app service name")."