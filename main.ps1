Param(
    [Parameter(Mandatory = $true)]
    [ValidateNotNullorEmpty()]
    [string]
    $location,
    [Parameter(Mandatory = $true)]
    [ValidateNotNullorEmpty()]
    [string]
    $resourceGroupName,
    [Parameter(Mandatory = $true)]
    [ValidateNotNullorEmpty()]
    [string]
    $TemplateFile,
    [Parameter(Mandatory = $true)]
    [ValidateNotNullorEmpty()]
    [string]
    $TemplateParameterFile,
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]
    $AzureSubscriptionName,
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]
    $ServicePrincipalID,
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]
    $ServicePrincipalKey,
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]
    $AzureTenantId
)

# Login Azure RM Account
$SecurePassword = $ServicePrincipalKey | ConvertTo-SecureString -AsPlainText -Force
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $ServicePrincipalID, $SecurePassword

Login-AzureRmAccount -ServicePrincipal -Credential $cred -TenantId $AzureTenantId

# Create new Resource Group
New-AzureRmResourceGroup -Location "$location" -Name "$resourceGroupName" -Force

# New Resource Group Deployment
New-AzureRmResourceGroupDeployment -ResourceGroupName $resourceGroupName -TemplateUri $TemplateFile -TemplateParameterFile $TemplateParameterFile -Verbose